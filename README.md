# hlf cluster on multiple hosts Hyperledger Fabric

    http://blog.hubwiz.com/2019/12/07/hyperledger-fabric-multi-hosts-network/
    http://blog.hubwiz.com/2020/03/12/fabric-2-external-chaincode/


Hyperledger Fabric区块链网络搭建已繁琐著称。本教程将介绍 如何部署一个分布在4个主机上包含多个排序节点和对等节点的 hyperledger fabric区块链集群网络，同时提供源码和配置文件下载。

1、服务结构
我们要搭建的Hyperledger Fabric网络拓扑结构如下：



网络包含如下服务：

1个机构：org1.example.com
3个对等节点：peer0.example.com、peer1.example.com、peer2.example.com
1个CA节点：ca.example.com
3个排序节点：order0.example.com、order1.example.com、order2.example.com
3个zookeeper节点：zookeeper0、zookeeper1、zookeeper2
4个kafka节点：kafka0、kafka1、kafka2、kafka3
2、部署服务
2.1 部署ca、orderer和kafka
如上图所示，首先在server1上部署CA节点、排序节点、kafka节点和zookeeper节点， 使用的docker-compose文件文件为docker-compose-kafka.yml：

# deploy ca, zookeerper, kafka and orderers on server0
docker-compose  -f deployment/docker-compose-kafka.yml up -d
在docker-compose文件中，我们为ca和orderer服务定义了extra_hosts属性， 其中包含了所有对等节点的信息。由于ca和orderer可能需要与对等节点通信， 因此它们需要了解对等节点的信息。peers部署在不同的主机上，因此我们 可以在extra_hosts字段定义peer0、peer1和peer2的主机。

extra_hosts:
    - "peer0.org1.example.com:172.31.26.5"
    - "peer1.org1.example.com:172.31.20.177"
    - "peer2.org1.example.com:172.31.27.143"
2.2 部署peer0和cli0
我们在server2上部署peer0和cli0，其中cli0将接入peer0。使用的 docker-compose文件为docker-compose-peer0.yml和docker-compose-cli.yml：

# deploy peer0
docker-compose -f deployment/docker-compose-peer0.yml up -d

# deploy cli0
docker-compose -f deployment/docker-compose-cli0.yml up -d
在docker-compose-peer0.yml中，我们定义了如下的extra_hosts字段， 其中包含了所有排序节点、peer1和peer2的主机信息。添加其他对等节点 信息的主要原因在于，对等节点使用gossip协议来向其他peer广播区块。

extra_hosts:
    - "orderer0.example.com:172.31.25.198"
    - "orderer1.example.com:172.31.25.198"
    - "orderer2.example.com:172.31.25.198"
    - "ca.example.com:172.31.25.198"
    - "peer1.org1.example.com:172.31.20.177"
    - "peer2.org1.example.com:172.31.27.143"
在docker-compose-cli0.yml中包含了排序节点的主机信息，因为cli命令 在执行交易时需要与排序节点通信：

extra_hosts:
  - "orderer0.example.com:172.31.25.198"
  - "orderer1.example.com:172.31.25.198"
  - "orderer2.example.com:172.31.25.198"
2.3 部署peer1和cli1
接下来我们在server3上部署peer1和cli1：

# deploy peer1
docker-compose -f deployment/docker-compose-peer1.yml up -d

# deploy cli1
docker-compose -f deployment/docker-compose-cli1.yml up -d
在docker-compose-peer1.yml中的extra_hosts中包含了排序节点主机 和其他对等节点主机（peer0和peer2）的信息。docker-compose-cli.yml 中的extra_hosts字段定义了排序节点的主机信息。

2.4 部署peer2和cli2
最后，我们在server4上部署peer2和cli2：

# deploy peer2
docker-compose -f deployment/docker-compose-peer2.yml up -d

# deploy cli2
docker-compose -f deployment/docker-compose-cli2.yml up -d
同样在docker-compose文件中需要指定extra_hosts。

3、配置通道
现在已经完成了服务部署，接下来就需要配置通道了。

3.1 创建通道
通过接入server2上的peer0，执行如下命令创建通道：

# create channel from peer0 on server2
# it connects to orderer0
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" \
            -e "CORE_PEER_MSPCONFIGPATH=/var/hyperledger/users/Admin@org1.example.com/msp" \
            peer0.org1.example.com peer channel create 
            -o orderer0.example.com:7050 -c mychannel -f /var/hyperledger/configs/channel.tx
接下来我们将把所有三个对等节点加入通道

3.2 将peer0加入通道
现在接入server2上的peer0，执行如下的命令：

# join peer0 to channel
# execute this command from server1
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" \
            -e "CORE_PEER_MSPCONFIGPATH=/var/hyperledger/users/Admin@org1.example.com/msp" \
            peer0.org1.example.com peer channel join -b mychannel.block
上面的命令将在peer0容器内生成mychannel.block。

3.3 将mychannel.block拷贝到peer1和peer2
# copy mychannel.block from peer0 to host(server2)
docker cp peer0.org1.example.com:/mychannel.block .

# transfer mychannel.block to server3 and server4 via scp
scp -r mychannel.block ubuntu@172.31.20.177:
scp -r mychannel.block ubuntu@172.31.27.143:

# copy mychannel.block to peer1 and peer2 
# peer1 is on server3
# peer2 is on server4
docker cp mychannel.block peer1.org1.example.com:/mychannel.block
docker cp mychannel.block peer2.org1.example.com:/mychannel.block

# remove mychannel.block from server2, server3 and server4
rm mychannel.block
3.4 将peer1加入通道
现在可以将server3上的peer1加入通道：

# join peer1 to channel
# execute this command from server3 machine
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/var/hyperledger/users/Admin@org1.example.com/msp" \
            peer1.org1.example.com peer channel join -b mychannel.block
3.5 将peer2加入通道
同样，可以将server4上的peer2加入通道：

# join peer2 to channel
# execute this command from server4 machine
docker exec -e "CORE_PEER_LOCALMSPID=Org1MSP" -e "CORE_PEER_MSPCONFIGPATH=/var/hyperledger/users/Admin@org1.example.com/msp" \
            peer2.org1.example.com peer channel join -b mychannel.block
4、配置链码
4.1 安装链码
链码在chaincode目录。我们使用cli在每个peer节点上安装链码。

# install on peer0 on server2
# cli container connects to peer0 
docker exec -it cli peer chaincode install -n mycc -p github.com/chaincode -v v0

# install on peer1 on server3
# cli container connects to peer1 
docker exec -it cli peer chaincode install -n mycc -p github.com/chaincode -v v0

# install on peer2 on server4
# cli container connects to peer2
docker exec -it cli peer chaincode install -n mycc -p github.com/chaincode -v v0
4.2 实例化链码
现在可以实例化链码了。在通道上只需要进行一次实例化。因此我们用 server2上的cli0进行链码实例化操作：

# instantiate chaincode from peer0 on server2
# it connects to orderer0 
docker exec -it cli peer chaincode instantiate -o orderer0.example.com:7050 \
            -C mychannel -n mycc github.com/chaincode -v v0 -c '{"Args": ["a", "100"]}'
5、交易的执行与查询
5.1 执行交易
我们使用server2上的cli0调用链码交易：

# invoke transaction from peer0 on server2
# it connects to orderer0 on server1
docker exec -it cli peer chaincode invoke -o orderer0.example.com:7050 \
            -n mycc -c '{"Args":["set", "a", "20"]}' -C mychannel
5.2 查询交易
我们使用server4上的cli2查询交易：

# query transaction from peer2 on server4
docker exec -it cli peer chaincode query \
            -n mycc -c '{"Args":["query","a"]}' -C mychannel
本教程的源代码及配置文件可以在这里下载。

原文链接：Hyperledger fabric cluster on multiple hosts

